# Theodor
[![pipeline status](https://gitlab.com/ucsdlibrary/development/theodor/badges/master/pipeline.svg)](https://gitlab.com/ucsdlibrary/development/theodor/-/commits/master)
[![coverage report](https://gitlab.com/ucsdlibrary/development/theodor/badges/master/coverage.svg)](https://gitlab.com/ucsdlibrary/development/theodor/-/commits/master)

A happy and wise Slack-bot for the UCSD Library, or so we've been told.

<img title="Theodor the wise owl" src="assets/dewey.png" width="200">

## What can it do?

### Events
1. The bot will post to the `#general` channel when a new channel is created.
1. The bot will welcome a new user to the team with a customized welcome message

### Commands
1. `hours` - responds with Library building(s) hours for the day
1. `service desk` - responds with options for contacting the service desk

In time, assuming interest, additional bot-like features will be added.

## API Token
The Slack bot requires an API token to connect to the Slack API.

We have a production token which should only be used for production. If you need
to test new features locally, you can use the `theodor-test` API token.

Note that this account is invited into the `bot-testing` channel.

This token can be found in Slack directly or in LastPass.

## Installing (docker)
1. Clone the repo: `git clone https://github.com/ucsdlib/theodor.git`
1. Build image: `docker build -t theodor .`
1. Run image: `docker run --rm -e "SLACK_API_TOKEN=<test-token>" theodor`

Run rubocop or tests:
1. `docker run --rm theodor bundle exec rubocop`
1. `docker run --rm theodor bundle exec rake`

## Installing (traditional)
1. Clone the repo: `git clone https://github.com/ucsdlib/theodor.git`
1. Install dependencies: `bundle install`
1. Run test suite `rake`. This will also run `rubocop` in addition to `rspec`
1. Run application `SLACK_API_TOKEN=<test-token> bundle exec puma`

## Kubernetes Deploy
Install Dependencies:
  - [k3d][k3d] (allows running a [k3s][k3s] cluster locally inside Docker containers)
  - Docker, Helm, and kubectl (other required dependencies)

1. Run the `./k3d-theodor.sh <api-token>` script, ensuring to pass the test API token as a
   parameter
1. You can navigate to the `bot-testing` channel to interact w/ the
   `theodor-test` bot instance, or directly via DM

[k3d]:https://github.com/rancher/k3d/
[k3s]:https://github.com/rancher/k3s/
## GitLab CI/CD
The following steps were taken to create a Helm/k8s based deployment for
Theodor.

1. A local user created on rancher cluster, added as a member of a new Rancher
   Project called `theodor`, with a production `namespace` within it named
   `theodor`, and a staging `namespace` called `theodor-staging`. These are set
   as `KUBE_NAMESPACE_PROD` and `KUBE_NAMESPACE_STAGING` variables.
1. The `kubeconfig` file was copied from Rancher for this user and turned into a
   format useable as a CI/CD environment variable (`KUBE_ROLE_CONFIG`) by doing
   the following: `cat $KUBECONFIG | base64`.
1. A Helm 3 docker image is used, as opposed to the GitLab `auto-deploy` image,
   because it is still stuck on Helm2 and thus would require installing a
   `tiller`.
1. There are `cleanup` CI jobs to delete Helm releases. These must be run
   manually for a given pipeline run, but can be used to remove staging
   releases, for example, if desired.

### How to deploy the staging/test Bot account
In order to deploy the staging/test Bot account, you will need to manually
trigger a pipeline, on your `branch`.

In the `Variables` section, you should define a variable named
`DEPLOY_TEST_BOT`. You can give it any value, but the `deploy_staging` job keys
off of the existence of this variable and some value defined, not a particular
value.

![Staging Deployment](./assets/stage-deploy.png)

When you are done reviewing your changes, please remember to run the
`cleanup_staging` job to remove the release.
